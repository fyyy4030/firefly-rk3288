package com.android.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class DeviceNameDialogFragment extends DialogFragment{
	private EditText mDeviceNameView;
	private ContentResolver mContentResolver;
	private AlertDialog mAlertDialog;
	
	private DeviceInfoSettings infoSettings;
    
    DeviceNameDialogFragment(DeviceInfoSettings info){
    	infoSettings = info;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContentResolver = getActivity().getContentResolver();
	}

	private View createDialogView(String deviceName) {
        final LayoutInflater layoutInflater = (LayoutInflater)getActivity()
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dialog_edittext, null);
        mDeviceNameView = (EditText) view.findViewById(R.id.edittext);
        mDeviceNameView.setText(deviceName);    // set initial value before adding listener
        return view;
    }
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String deviceName = Settings.System.getString(mContentResolver, Settings.System.DEVICE_NAME);
        mAlertDialog = new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(R.string.bluetooth_rename_device)
                .setView(createDialogView(deviceName))
                .setPositiveButton(R.string.bluetooth_rename_button,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Settings.System.putString(mContentResolver, Settings.System.DEVICE_NAME, mDeviceNameView.getText().toString());
                                infoSettings.updateName();
                                mAlertDialog.dismiss();
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        mAlertDialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        return mAlertDialog;
    }
}
